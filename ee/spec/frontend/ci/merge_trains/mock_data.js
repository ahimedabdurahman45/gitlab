// Fixture located at ee/spec/frontend/fixtures/merge_trains.rb
import activeTrain from 'test_fixtures/ee/graphql/merge_trains/active_merge_trains.json';
import mergedTrain from 'test_fixtures/ee/graphql/merge_trains/completed_merge_trains.json';

export { activeTrain, mergedTrain };
